<?php

require_once __DIR__ . "/services.php";

header('Content-Type: application/json');

try {

    $json = file_get_contents('php://input');
    $post = json_decode($json);

} catch (Exception $e) {

    http_response_code(400);

    die(json_encode([
        "error" => true,
        "message" => "body must be in json format"
    ]));

}

if ($_SERVER["REQUEST_METHOD"] !== "POST" || !isset($post->type) || !isset($post->data)) {

    http_response_code(400);

    die(json_encode([
        "error" => true,
        "message" => "no valid request method"
    ]));

}

$success = false;

switch ((string) $post->type) {

case 'webpage':
    $success = insert_new_webpage_statistic((object) $post->data);
    break;

case 'app':
    $success = insert_new_app_statistic((object) $post->data);
    break;

}

if ($success) {
    
    die(json_encode([
        "error" => false,
        "message" => "added successfully"
    ]));

}

http_response_code(400);
    
die(json_encode([
    "error" => true,
    "message" => "error when adding new statistic"
]));