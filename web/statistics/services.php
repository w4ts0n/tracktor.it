<?php

require_once __DIR__ . "/log.php";

$db = new PDO("sqlite:" . __DIR__ . "/database.db");

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$config = (object) [
    "spam_protection" => (object) [
        "entries_per_minute" => 10, // max. x single generated writing per minute
        "max_trackers_per_entrie" => 28 // max. x tracker per single generated writing 
    ]
];

// entry_id => # ID to assign multiple trackers to a single generation

$db->exec(
    "CREATE TABLE IF NOT EXISTS app (
        `id` INTEGER PRIMARY KEY,
        `entry_id` varchar(13) NOT NULL, 
        `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, 
        `action` varchar(10) NOT NULL,
        `app_id` text NOT NULL,
        `app_version` varchar(30) NOT NULL,
        `tracker_slug` text NOT NULL,
        `supervisory_authority_slug` varchar(30)
    )");


$db->exec(
    "CREATE TABLE IF NOT EXISTS webpage (
        `id` INTEGER PRIMARY KEY, 
        `entry_id` varchar(13) NOT NULL,
        `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, 
        `action` varchar(10) NOT NULL,
        `domain` text NOT NULL,
        `tracker_slug` text NOT NULL,
        `supervisory_authority_slug` varchar(30)
    )");

# supervisory_authority_slug was added later, so must be added again for old databases.
# Can be removed again in the future
$result = $db->query("SELECT * FROM `webpage` LIMIT 1");
$data = $result->fetchAll();
$data = array_keys($data[0]);
if (!in_array("supervisory_authority_slug", $data, true)) {
    $db->exec("ALTER TABLE `app` ADD COLUMN `supervisory_authority_slug` varchar(30);");
    $db->exec("ALTER TABLE `webpage` ADD COLUMN `supervisory_authority_slug` varchar(30);");
}


function is_spam_protection_triggered (string $table): bool {
    global $db, $config;

    $stmt = $db->query("SELECT id FROM `$table` WHERE `created_at` > datetime('now','-1 minutes') GROUP BY entry_id");
    $entries_last_minute = $stmt->fetchAll();

    if (count($entries_last_minute) > $config->spam_protection->entries_per_minute) {
        return true;
    }

    return false;    

}

function get_uniqid ($length = 13) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getuniq_entry_id (string $table): string {
    global $db;
    $entrie_id = "";

    for ($tries = 0; $tries < 100; $tries++) {

        $entrie_id = get_uniqid();
        $count_entries = 1;

        $stmt = $db->prepare("SELECT COUNT(*) FROM `$table` WHERE `entry_id` = ?");
        if ($stmt) $stmt->execute(array($entrie_id));
        if ($stmt) $stmt = $stmt->fetchAll();
        if ($stmt) $count_entries = (int) $stmt[0]["COUNT(*)"];
    
        if ($count_entries === 0) {
            break;
        }

    }

    return $entrie_id;

}

// -- Example data object for a new web page statistics --

// "data": {
//     "url": "https://test.google.com/api/test",
//     "action": "complaint",
//     "tracker_slugs": [
//         "adobe",
//         "facebook"
//     ],
//     "supervisory_authority_slug": "debawueldb"
// }

function insert_new_webpage_statistic (object $data): bool {
    global $db, $config;

    if (is_spam_protection_triggered("webpage")) {
        log_error("spam_protection_triggered (webpages)");
        return false;
    }

    if (!isset($data->tracker_slugs) || 
        !isset($data->action) ||
        !isset($data->url)
    ) {
        log_error("bat data (webpages)");
        return false;
    }

    $domain = htmlentities(parse_url((string) $data->url, PHP_URL_HOST));
    $domain = substr($domain, 0, 50); // cut off values too long 

    $action = (string) $data->action;

    if ($action !== "complaint" && $action !== "request") {
        log_error("bad action (webpages)");
        return false;
    }

    if (gettype($data->tracker_slugs) !== "array") {
        log_error("bad type of tracker_slugs (webpages)");
        return false;
    }

    // currently there are about $ trackers, 
    // so it is very unlikely that more were found 
    if (count($data->tracker_slugs) > $config->spam_protection->max_trackers_per_entrie) {
        log_error("tracker_slugs > trackers (webpages)");
        return false;
    }

    $entrie_id = getuniq_entry_id('webpage');
    $supervisory_authority_slug = htmlentities((string) $data->supervisory_authority_slug);

    foreach ($data->tracker_slugs as &$tracker_slug) {
        $tracker_slug = htmlentities((string) $tracker_slug);

        $stmt = $db->prepare('INSERT INTO webpage (`action`, `domain`, `tracker_slug`, `entry_id`, `supervisory_authority_slug`) VALUES (?, ?, ?, ?, ?)');

        if (!$stmt || !$stmt->execute(array($action, $domain, $tracker_slug, $entrie_id, $supervisory_authority_slug))) {
            log_error("insert new item (webpages)");
            return false;
        }
        
    }

    return true;

}

// -- Example data object for a new web page statistics --

// "data": {
//     "app_id": "testapp.de",
//     "app_version": "5.1",
//     "action": "complaint",
//     "tracker_slugs": [
//         "adobe",
//         "facebook"
//     ],
//     "supervisory_authority_slug": "debawueldb"
// }

function insert_new_app_statistic (object $data): bool {
    global $db, $config;

    if (is_spam_protection_triggered("app")) {
        log_error("spam_protection_triggered (app)");
        return false;
    }

    if (!isset($data->tracker_slugs) || 
        !isset($data->action) ||
        !isset($data->app_id) ||
        !isset($data->app_version)
    ) {
        log_error("bat data (app)");
        return false;
    }

    $action = (string) $data->action;
    $app_id = substr(htmlentities((string) $data->app_id), 0, 50);
    $app_version = substr(htmlentities((string) $data->app_version), 0, 50);

    if ($action !== "complaint" && $action !== "request") {
        log_error("bad action (app)");
        return false;
    }

    if (gettype($data->tracker_slugs) !== "array") {
        log_error("bad type of tracker_slugs (app)");
        return false;
    }

    // currently there are about $ trackers, 
    // so it is very unlikely that more were found 
    if (count($data->tracker_slugs) > $config->spam_protection->max_trackers_per_entrie) {
        log_error("tracker_slugs > trackers (app)");
        return false;
    }

    $entrie_id = getuniq_entry_id('app');
    $supervisory_authority_slug = htmlentities((string) $data->supervisory_authority_slug);

    foreach ($data->tracker_slugs as &$tracker_slug) {
        $tracker_slug = htmlentities((string) $tracker_slug);

        $stmt = $db->prepare('INSERT INTO app (`action`, `app_id`, `app_version`, `tracker_slug`, `entry_id`, `supervisory_authority_slug`) VALUES (?, ?, ?, ?, ?, ?)');

        if (!$stmt->execute(array($action, $app_id, $app_version, $tracker_slug, $entrie_id, $supervisory_authority_slug))) {
            log_error("insert new item (app)");
            return false;
        }
        
    }

    return true;

}


function get_count_of_generation (): object {
    global $db;
    
    // "SELECT 1" and not "SELECT COUNT(*)" because of the GROUP BY

    $res = (object) [
        "generated_requests" => 0,
        "generated_complaints" => 0
    ];
    
    foreach (array("app", "webpage") as &$table) {

        $stmt = $db->query("SELECT 1 FROM `$table` WHERE `action` = 'complaint' GROUP BY entry_id");
        if ($stmt) $stmt = $stmt->fetchAll();
        if ($stmt) $res->generated_complaints += count($stmt);
        
        $stmt = $db->query("SELECT 1 FROM `$table` WHERE `action` = 'request' GROUP BY entry_id");
        if ($stmt) $stmt = $stmt->fetchAll();
        if ($stmt) $res->generated_requests += count($stmt);

    }
    
    return $res;

}

function get_count_of_unique_entries (): object {
    global $db;
    
    // "SELECT 1" and not "SELECT COUNT(*)" because of the GROUP BY

    $res = (object) [
        "entries_total" => 0,
        "unique_domains" => 0,
        "unique_apps" => 0
    ];

    $stmt = $db->query("SELECT 1 FROM `app` GROUP BY entry_id");
    if ($stmt) $stmt = $stmt->fetchAll();
    if ($stmt) $res->entries_total += count($stmt);
    
    $stmt = $db->query("SELECT 1 FROM `webpage` GROUP BY entry_id");
    if ($stmt) $stmt = $stmt->fetchAll();
    if ($stmt) $res->entries_total += count($stmt);

     
    $stmt = $db->query("SELECT 1 FROM `webpage` GROUP BY domain");
    if ($stmt) $stmt = $stmt->fetchAll();
    if ($stmt) $res->unique_domains = count($stmt);
    
    $stmt = $db->query("SELECT 1 FROM `app` GROUP BY app_id");
    if ($stmt) $stmt = $stmt->fetchAll();
    if ($stmt) $res->unique_apps += count($stmt);
    
    return $res;

}

function get_supervisory_authorities_usage (): object {
    global $db;

    $res = (object) [];

    foreach (array("app", "webpage") as &$table) {

        $stmt = $db->query("SELECT supervisory_authority_slug as slug, _count as count FROM (SELECT supervisory_authority_slug, count(*) AS _count FROM (SELECT * FROM $table WHERE `action` = 'complaint' GROUP BY entry_id) GROUP BY `supervisory_authority_slug`);");
        if ($stmt) $stmt = $stmt->fetchAll();
        if ($stmt) {
            foreach ($stmt as &$row) {
                if (property_exists($res, $row["slug"])) {
                    $res->{$row["slug"]} += $row["count"];
                } else {
                    $res->{$row["slug"]} = $row["count"];
                }
            }
        }

    }

    return $res;

}

function get_tracker_usage (): object {
    global $db;

    $res = (object) [
        "webpage" => [],
        "app" => []
    ];

    $stmt = $db->query("SELECT `tracker_slug`, COUNT(*) as count FROM `webpage` GROUP BY `tracker_slug` ORDER BY 2 DESC");
    if ($stmt) $stmt = $stmt->fetchAll();
    if ($stmt) {
        $res->webpage = $stmt;
    }

    $stmt = $db->query("SELECT `tracker_slug`, COUNT(*) as count FROM `app` GROUP BY `tracker_slug` ORDER BY 2 DESC");
    if ($stmt) $stmt = $stmt->fetchAll();
    if ($stmt) {
        $res->app = $stmt;
    }

    return $res;

}


function get_captured_websites_and_apps (string $query, string $type, string $action, string $supervisory_authority): array {
    global $db;

    $complete_list = [];

    $unsafe_sql_add = "";
    $sql_execute = [
        "query" => "%$query%"
    ];
    
    if ($action !== "") {
        $unsafe_sql_add .= " `action` = :action AND ";
        $sql_execute["action"] = $action;
    }

    if ($supervisory_authority !== "") {
        $unsafe_sql_add .= " `supervisory_authority_slug` = :supervisory_authority_slug AND ";
        $sql_execute["supervisory_authority_slug"] = $supervisory_authority;
    }

    if (strpos($type, "webpage") !== false) {
        $stmt = $db->prepare("SELECT * FROM `webpage` WHERE domain != '[hidden]' AND $unsafe_sql_add domain LIKE :query GROUP BY entry_id");
        $stmt->execute($sql_execute);

        if ($stmt) $stmt = $stmt->fetchAll();
        if ($stmt) $complete_list = array_merge($complete_list, $stmt);
    }
    
    if (strpos($type, "app") !== false) {
        $stmt = $db->prepare("SELECT * FROM `app` WHERE app_id != '[hidden]' AND $unsafe_sql_add app_id LIKE :query GROUP BY entry_id");
        $stmt->execute($sql_execute);

        if ($stmt) $stmt = $stmt->fetchAll();
        if ($stmt) $complete_list = array_merge($complete_list, $stmt);
    }

    usort($complete_list, function ($a, $b) {
        return -strcmp(get_unixtime($a["created_at"]), get_unixtime($b["created_at"]));
    });

    return $complete_list;

}

function get_unixtime ($dbtime) {

    $date = date_parse($dbtime);

    return mktime($date["hour"], $date["minute"], $date["second"], $date["month"], $date["day"], $date["year"]);

}

function get_date_from_db_time(string $dbtime): string {

    $date = date_parse($dbtime);

    $day = $date["day"];
    if ($day < 10) $day = "0$day";

    $month = $date["month"];
    if ($month < 10) $month = "0$month";

    return $day . "." . $month . "." . $date["year"];

}


function get_all_entries_for_admin (): array {
    global $db;

    $stmt = $db->query("SELECT * FROM `webpage`");
    $webpages = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $stmt = $db->query("SELECT * FROM `app`");
    $apps = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return (array) [
        "webpage" => $webpages,
        "app" => $apps
    ];

}

function delete_complete_entry (string $unsave_table, string $unsave_entry_id) {
    global $db;

    if ($unsave_table !== "webpage" && $unsave_table !== "app")
        return false;

    $table = $unsave_table;

    $stmt = $db->prepare("DELETE FROM `$table` WHERE entry_id = ?");

    if ($stmt->execute(array($unsave_entry_id))) {
        return true;
    } else {
        return false;
    }

}