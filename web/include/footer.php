<?php if (!$included) die(); ?>

<footer class="bg-secondary w-full mt-32">

    <div class="max-w-screen-lg md:flex justify-between mx-auto pt-10 pb-14 px-4">
        <div class="mr-auto">
            <img class="w-auto h-7" src="assets/images/tracktor_logo_type.svg" alt="tracktor.it">

        </div>

        <ul class="mx-auto mt-10 md:mt-0">
            <li>
				<a rel="me" href="https://chaos.social/@tracktor" style="white-space:nowrap">Mastodon
					<img style="vertical-align:middle; display:inline-block;" src="assets/icons/mastodon_16.png" alt="Mastodon" /> 
				</a>
			</li>            
            <li class="leading-8"><a href="/imprint.php"><?php echo t("navigation.imprint"); ?></a></li>
            <li class="leading-8"><a href="/imprint.php"><?php echo t("navigation.privacy"); ?></a></li>

            <div style="display:inline-block;width:30px;text-align:center"></div>

            
        </ul>
        <ul class="ml-auto mt-10 md:mt-0">
            <li class="leading-8"><a href="https://codeberg.org/rufposten/tracktor.it"><?php echo t("navigation.sourcecode"); ?></a></li>
            <li class="leading-8"><a href="/statistics.php"><?php echo t("navigation.captured-websites-and-apps"); ?></a></li>
            <li class="leading-8"><a href="/supervisory-authority.php"><?php echo t("navigation.supervisory-authority"); ?></a></li>
            <li class="leading-8"><a href="https://codeberg.org/rufposten/tracktor.it/issues"><?php echo t("navigation.add-tracker"); ?></a></li>
            <li class="leading-8"><a href="https://codeberg.org/rufposten/tracktor.it/src/data/localization"><?php echo t("navigation.help-with-translation"); ?></a></li>
        </ul>
    </div>

</footer>

<script src="assets/js/main.js"> </script>


<!-- START: DEV ONLY -->
<!-- This is automatically removed by the build script. -->

<base href="http://localhost:3000/">

<script>

    setInterval(() => {
        a = [...document.querySelectorAll("a")];
        a.forEach(item => {
            if (item.getAttribute("href").indexOf(location.port) === -1 && !item.getAttribute("href").startsWith("https://")) {
                item.setAttribute("href", "http://" + location.host + item.getAttribute("href"))
            }
        })
    }, 1000);

</script>

<script src="static/js/bundle.js"></script>
<script src="static/js/0.chunk.js"></script>
<script src="static/js/1.chunk.js"></script>
<script src="static/js/main.chunk.js"></script>

<!-- END: DEV ONLY -->
