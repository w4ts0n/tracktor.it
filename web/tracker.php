<?php

require_once "./core/localization.php";

$included = true;
$html_title = t("tracker.title");

require "./include/header.php";
require "./core/utils.php";
require "./statistics/services.php";

$trackers = get_tracker_data();
$tracker_usage = (array) get_tracker_usage();

?>

<body>

    <?php require_once "./include/navbar.php" ?>

    <div class="max-w-screen-lg mx-auto px-6 lg:px-0">

        <h1 class="text-primary text-3xl md:text-4xl font-medium"><?php echo t("tracker.title"); ?></h1>
        <p class="my-3 text-lg"> <?php echo t("tracker.desc"); ?> </p>
        <a class="link text-right" href="https://codeberg.org/rufposten/tracktor.it/issues" target="_blank" rel="noopener noreferrer">
            <?php echo t("tracker.add-new-tracker"); ?>
        </a>
		<div class="h-10"></div>

        <input onkeyup="filterList(this)" class="my-3 rounded border-2 px-4 py-2 w-full sm:w-1/2 focus:border-primary" placeholder="<?php echo t("tracker.filter-placeholder"); ?>"/>

        <ul class="list" style="min-height: 50vh">
            <li class="notfound hidden border-2 border-gray-100 p-10 my-8">
                <h3 class="text-2xl font-medium line-break"><?php echo t("tracker.nomatch"); ?></h3>
            </li>

<?php foreach ($trackers as $key => $tracker): ?>

            <li class="border-2 border-gray-100 p-10 my-8 relative" data-search="<?php echo strtolower($tracker["name"] . join("", $tracker["tracking-urls"])); ?>">

                <h3 class="text-2xl font-medium line-break">
                    <?php echo $tracker["name"]; ?>                  
                </h3>

                <p class='my-2 text-xs text-gray-500'>
                <?php 
                $tracker_usage_by_type = (array) $tracker_usage[$tracker["type_short"]];
                
                $key = array_search($tracker["slug"], array_column($tracker_usage_by_type, 'tracker_slug'));

                if ($key !== false) {
                    echo "Wird in " . $tracker_usage_by_type[$key]["count"] . " Schreiben verwendet.";
                } else {
                    echo "Wurde noch nicht verwendet.";
                }
                
                ?>
                </p>

                <div class="absolute top-10 right-10 px-4 py-2 bg-secondary rounded">
                    <?php echo $tracker["type"]; ?>
                </div>

                <?php if (isset($tracker["customInput"])):
                    echo t("tracker.cname-cloaking");
                else:
                    echo join(",<br />", $tracker["tracking-urls"]);
                endif;?>

                <p class="text-primary cursor-pointer my-2" onclick="toggleDetails('<?php echo $tracker["slug"] ?>')">
                    <span class="hidden" data-searchme="<?php echo $tracker["slug"] ?>"><?php echo t("tracker.hide-details"); ?></span>
                    <span data-searchme="<?php echo $tracker["slug"] ?>"><?php echo t("tracker.show-details"); ?></span>
                </p>

                <div class="hidden" data-searchme="<?php echo $tracker["slug"] ?>">
                    <p class="line-break">
                        <?php echo preg_replace('/^(.*?)\n/', "", $tracker["template"], 1); ?>
                    </p>
<?php if (isset($tracker["note"])): ?>
                    <div>
                    <h3 class="text-2xl font-medium line-break mt-10"><?php echo $tracker["note"]["title"]; ?></h3>
                        <p class="line-break"><?php echo $tracker["note"]["text"]; ?>}</p>
                        <img width="100%" alt="<?php echo $tracker["note"]["title"]; ?>" src="assets/<?php echo $tracker["note"]["image"]; ?>"/>
                    </div>
<?php endif; ?>
                </div>
            </li>

<?php endforeach; ?>
        </ul>

    </div>

    <script src="assets/js/search.js"> </script>

    <?php require_once "./include/footer.php" ?>

</body>
</html>
