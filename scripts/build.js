const { exec } = require("child_process");
const path = require("path");
const fs = require("fs");

const deleteFolderRecursive = function(dir) {
    if (fs.existsSync(dir)) {
        fs.readdirSync(dir).forEach((file, index) => {
            const curPath = path.join(dir, file);
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(dir);
    }
};
var copyRecursiveSync = function(src, dest) {
    var exists = fs.existsSync(src);
    var stats = exists && fs.statSync(src);
    var isDirectory = exists && stats.isDirectory();
    if (isDirectory) {
        if (!fs.existsSync(dest)) {
            fs.mkdirSync(dest);
        }
      fs.readdirSync(src).forEach(function(childItemName) {
        copyRecursiveSync(path.join(src, childItemName),
                          path.join(dest, childItemName));
      });
    } else {
      fs.copyFileSync(src, dest);
    }
};

async function run (command) {
    return new Promise((re, rj) => {

        exec(command, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                rj();
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                rj();
                return;
            }
            console.log(stdout);
            re();
        });

    })
    
}

(async ()=>{

    console.log("> Creating production build...");

    const dataScipt = path.join(__dirname, "..", "data", "build-data.js");
    await run("node " + dataScipt);

    const generatorPath = path.join(__dirname, "..", "generator");
    await run(`cd '${generatorPath}' && npm run build`);

    const buildPath = path.join(__dirname, "..", "build");
    deleteFolderRecursive(buildPath);

    fs.mkdirSync(buildPath);

    copyRecursiveSync(path.join(generatorPath, "build", "static"), path.join(buildPath, "static"));
    copyRecursiveSync(path.join(__dirname, "..", "web"), path.join(buildPath));

    const dbPath = path.join(buildPath, "statistics", "database.db");
    if (fs.existsSync(dbPath)) 
        fs.unlinkSync(dbPath);

    const configPHP = path.join(buildPath, "admin", "config.php");
    if (fs.existsSync(configPHP)) 
        fs.unlinkSync(configPHP);

    const footerPath = path.join(buildPath, "include", "footer.php");
    const headerPath = path.join(buildPath, "include", "header.php");
    const indexPath = path.join(generatorPath, "build", "index.html");

    let headerContent = fs.readFileSync(headerPath).toString();
    let footerContent = fs.readFileSync(footerPath).toString();
    let newContent = fs.readFileSync(indexPath).toString().split("</head>");

    newContent[0] = newContent[0].slice(6)
    newContent[1] = newContent[1].slice(26)

    headerContent = headerContent.replace(/<\/head>/g, newContent[0] + "</head>");
    footerContent = footerContent.replace(/<!-- START: DEV ONLY -->((.|[\n])*?)<!-- END: DEV ONLY -->/g, newContent[1]);

    fs.writeFileSync(footerPath, footerContent);
    fs.writeFileSync(headerPath, headerContent);

    console.log("> Production build created...");

})()