<?php 

echo "ATTENTION!\n";
echo "The date must be read in the correct format. It is best to call correct-times.php again afterwards...\n";

die("and now die() can be removed");


$db = new PDO("sqlite:" . __DIR__ . "/database.db");

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$config = (object) [
    "spam_protection" => (object) [
        "entries_per_minute" => 10, // max. x single generated writing per minute
        "max_trackers_per_entrie" => 20 // max. x tracker per single generated writing 
    ]
];

$db->exec(
    "CREATE TABLE IF NOT EXISTS app (
        `id` INTEGER PRIMARY KEY,
        `entry_id` varchar(13) NOT NULL, 
        `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, 
        `action` varchar(10) NOT NULL,
        `app_id` text NOT NULL,
        `app_version` varchar(30) NOT NULL,
        `tracker_slug` text NOT NULL
    )");


$db->exec(
    "CREATE TABLE IF NOT EXISTS webpage (
        `id` INTEGER PRIMARY KEY, 
        `entry_id` varchar(13) NOT NULL,
        `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, 
        `action` varchar(10) NOT NULL,
        `domain` text NOT NULL,
        `tracker_slug` text NOT NULL
    )");


$webpages_request = [
    "hessnatur.de" => "13.06.2020",
    "www.schenker-tech.de" => "27.06.2020",
    "bettenrid.de" => "27.06.2020",
    "wg-gesucht.de" => "27.06.2020",
    "sportscheck.com" => "20.07.2020",
    "ebay-kleinanzeigen.de" => "26.07.2020",
    "bmsofort.de" => "28.07.2020",
    "manufactum.de" => "11.08.2020",
    "cherry.de" => "11.08.2020",
    "shellfire.de" => "25.08.2020",
    "bittl.de" => "16.10.2020",
    "audible.de" => "15.11.2020",
    "ing.de" => "17.11.2020",
    "sport-schuster.de" => "17.11.2020",
    "mercedes-benz-bank.de" => "19.11.2020",
    "dict.leo.org" => "29.11.2020",
    "www.listnride.de" => "09.12.2020",
    "mueller.legal" => "09.12.2020",
    "www.gastro-hero.de" => "13.02.2021",
    "www.bikeexchange.de" => "14.02.2021",
    "onomotion.com " => "15.02.2021",
    "www.boels.de" => "18.02.2021",
    "www.outdoortrends.de" => "19.02.2021",
    "wematik.de" => "28.02.2021",
    "www.agrieuro.de" => "01.03.2021",
    "www.peugeot.de" => "03.03.2021",
    "ecomento.de" => "04.03.2021",
    "www.zarges.com" => "05.03.2021",
    "www.gadget-rausch.de" => "07.03.2021",
    "www.bartels-shop.com " => "07.03.2021",
    "www.jh-profishop.de" => "08.03.2021",
    "www.simfisch.de " => "09.03.2021",
    "www.firmenauto.de" => "10.03.2021",
    "autonotizen.de" => "11.03.2021",
    "www.snappcar.de" => "11.03.2021",
    "mobile.de" => "12.03.2021",
    "www.elektrofahrrad-einfach.de" => "13.03.2021",
    "www.trendsderzukunft.de" => "13.03.2021",
    "outdoorartikel24.de" => "14.03.2021",
    "www.deinhaengesessel.de" => "14.03.2021",
    "www.deralp.de" => "15.03.2021",
    "de.forumhome.com" => "15.03.2021",
    "www.kuechengoetter.de" => "16.03.2021",
    "teslamag.de" => "17.03.2021",
    "evolveskateboards.de" => "17.03.2021",
    "transoplast.de" => "18.03.2021",
    "www.auto-medienportal.net" => "19.03.2021",
    "www.ab-in-die-box.de" => "19.03.2021",
    "www.race-and-fun.com" => "20.03.2021",
    "brillen.de" => "21.03.2021",
    "soq.de" => "21.03.2021",
    "www.ultraleicht-trekking.com" => "22.03.2021"
];

$webpages_complaint = [
    "www.schenker-tech.de" => "24.07.2020",
    "bettenrid.de" => "20.11.2020",
    "sportscheck.com" => "06.10.2020",
    "bmsofort.de" => "13.08.2020",
    "cherry.de" => "11.09.2020",
    "shellfire.de" => "21.09.2020",
    "bittl.de" => "11.01.2021",
    "sport-schuster.de" => "10.03.2021",
    "mercedes-benz-bank.de" => "26.11.2020",
    "dict.leo.org" => "17.02.2021",
    "mueller.legal" => "14.02.2021",
    "www.gastro-hero.de" => "13.03.2021",
    "onomotion.com " => "12.03.2021",
    "www.outdoortrends.de" => "23.02.2021",
    "www.zarges.com" => "08.03.2021"
];

function get_uniqid ($length = 13) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function import_webpages ($webpages, $action) {
    global $db;

    foreach ($webpages as $domain => $datum) {

        $datum = strrev(str_replace(".", "-", $datum)) . " 00:00:00";
    
        $entrie_id = get_uniqid();
    
        $stmt = $db->prepare('INSERT INTO webpage (`action`, `domain`, `tracker_slug`, `entry_id`, `created_at`) VALUES (?, ?, ?, ?, ?)');
    
        if (!$stmt || !$stmt->execute(array($action, $domain, "", $entrie_id, $datum))) {
            print("ERROR: " . $domain . "\n");
        }
    
    }

}

import_webpages($webpages_request, "request");
import_webpages($webpages_complaint, "complaint");


$apps = [
    "de.mobilesoftwareag.clevertanken" => "25.06.2020"
];

foreach ($apps as $app_id => $datum) {

    $datum = strrev(str_replace(".", "-", $datum)) . " 00:00:00";

    $entrie_id = get_uniqid();

    $stmt = $db->prepare('INSERT INTO app (`action`, `app_id`, `app_version`, `tracker_slug`, `entry_id`, `created_at`) VALUES (?, ?, ?, ?, ?, ?)');

    if (!$stmt || !$stmt->execute(array("request", $app_id, "", "", $entrie_id, $datum))) {
        print("ERROR: " . $app_id . "\n");
    }

}
