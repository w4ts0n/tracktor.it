module.exports = {
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*"
    }
  },
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
};
