#!/bin/bash

if [ "$1" == "startdev" ]; then

    cd /home/data
    npm install &&
        npm start &

    cd /home/web
    php -S 0.0.0.0:9090 &

    cd /home/generator
    npm install &&
        npm start &

    /bin/bash

    exit

fi

if [ "$1" == "buildprod" ]; then

    cd /home/data
    npm install

    cd /home/generator
    npm install

    cd /home/scripts
    node build.js

    exit

fi

if [ "$1" == "b" ]; then
    docker build -t tracktorit .
    exit
fi

command=startdev
if [ "$1" == "bp" ]; then
    command=buildprod
fi


docker run -it -w "/home" -v "$(pwd)/":/home -p 9090:9090 -p 3000:3000 tracktorit ./docker.sh $command