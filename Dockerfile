FROM ubuntu:latest

RUN apt update
ENV DEBIAN_FRONTEND noninteractive
RUN apt install curl php php-sqlite3 -y

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt install nodejs  -y

WORKDIR "/home"